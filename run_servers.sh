#!/bin/bash

KAFKA_UP=$(docker ps -q --filter name=kafka)
if [[ -z ${KAFKA_UP} ]]; then
    echo "Starting Kafka"
    docker run -d -p 9092:9092 -p 2181:2181 -h kafka --name kafka massenz/kafka:0.9.0.1
else
    echo "Kafka container is already running"
fi

VERSION=1.10.19
# IF the image is missing, use this to build:
#   docker build -t massenz/voldemort:${VERSION} -f voldemort/Voldemort.Dockerfile voldemort/
#
VOLDEMORT_IMAGE=$(docker images | grep massenz/voldemort | grep ${VERSION})
if [[ -z ${VOLDEMORT_IMAGE} ]]; then
    docker build -t massenz/voldemort:${VERSION} -f voldemort/Voldemort.Dockerfile voldemort/
fi

VOLDEMORT_UP=$(docker ps -q --filter name=voldemort)
if [[ -z ${VOLDEMORT_UP} ]]; then
    echo "Running primary Voldemort, forwarding port: 6666"
    docker run -d -P -p 6666:6666 -h voldemort --name voldemort \
        massenz/voldemort:${VERSION}  "/etc/voldemort"

    echo "Running secondary Voldemort, forwarding port: 9666"
    docker run -d -P -p 9666:9666 -h voldemort.remote --name voldemort.remote \
        massenz/voldemort:${VERSION} "/etc/voldemort/secondary"
    
    echo "Running Applicator Cluster, forwarding port: 9866"
    docker run -d -P -p 9866:9866 -h voldemort.applicator --name voldemort.applicator \
        massenz/voldemort:${VERSION}  "/etc/voldemort/applicator"
else
    echo "Voldemort servers are already running"
fi

docker ps
