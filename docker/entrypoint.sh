#!/bin/bash
#
# API Server entry point script.
# Created by M. Massenzio, 2016-04-10

declare -r VOLDEMORT_LIB="/opt/voldemort/lib"

# Enables the remote Java debugging options, if the variable
# $DEBUG is defined; to enable, use:
#	docker run ... --env DEBUG=1 massenz/apiserver:0.1.0
#
# See also: https://medium.com/@lhartikk/development-environment-in-spring-boot-with-docker-734ad6c50b34#.89jlffhgv
if [[ -n ${DEBUG} ]]; then
	DEBUG_OPT="-Xdebug -Xrunjdwp:server=y,transport=dt_socket,suspend=n"
fi
echo "Debugging options: ${DEBUG_OPT}"
java ${DEBUG_OPT} -cp ${VOLDEMORT_LIB} -jar daynite.jar $@
