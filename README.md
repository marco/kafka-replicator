# Kafka Replication

Simple experimental code, using Kafka to replicate across V* instances.

While V* has an internal replication mechanism, we may want to sometimes augment that
with replicaiton policies (eg, when going across DC's that cross regulated boundaries)
and/or apply business logic to replicated fields.

The code in this repo will set up two "clusters" (initially made up of single V* instances)
and a Kafka queue between them.

## Voldemort Container

Creates a simple Voldemort container and adds a `issues` collection.

### Usage

To build and run::

    $ docker build -t massenz/voldemort -f voldemort/Voldemort.Dockerfile voldemort/
    $ docker run -h voldemort --rm -it --name voldemort -p 8089:8081 massenz/voldemort

This will expose port 8089 on the VM for the HTTP server, so you can access the Web UI via
`http://docker.local:8089`.

From another shell, you can connect the Voldemort client:

    $ docker run --rm -it --link voldemort massenz/voldemort ./bin/voldemort-shell.sh \
         issues tcp://voldemort:6666

and then execute commands against the `issues` collection::

    > put "9b6180e5-1217-469c-ae51-793f878d164a" \
            {"id": "9b6180e5-1217-469c-ae51-793f878d164a", "type": "BUG", "reporter": "marco", \
             "assignee": "", "watchers": [  "luisa" ], "title": "A bug bit me", "comments": [ \
             {"content": "This is a serious problem",  "commenter": "marco",  "created": 1459888164858  }, \
             {"content": "Let's dig deeper",  "commenter": "luisa",  "created": 1459889081423  } ], \
             "created": 1459888164861, "updated": 1459888164861 }
    > get "9b6180e5-1217-469c-ae51-793f878d164a"
    version(0:1) ts:1459890117304: {"comments":[{"content":"This is a serious problem", ... "updated":-3, }

## Kafka Container

Use a standard Kafka container, with default configurations.

We are currently using the [massenz/kafka:0.9.0.1](https://github.com/massenz/docker-kafka) image;
it may need some tweaking (especially around managing IP and networking).

### Usage

Launch the "kafka" container, this will run the server:

    # Launch the "main" Kafka container, this runs the server;
    # it also changes the `hostname` to "kafka" and sets an appropriate entry to /etc/hosts:
    $  docker run --rm --env ADVERTISED_HOST=kafka --env ADVERTISED_PORT=9092 \
            -p 9092:9092 -p 2181:2181 -h kafka \
            --name kafka massenz/kafka:0.9.0.1

To launch the Producer (this used to fail with a `kafka.common.LeaderNotAvailableException`
when posting to a topic):

    $ bin/kafka-console-producer.sh --broker-list localhost:9092 --topic data_has_changed

Similarly, the consumer can be run with:

    $ bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic data_has_changed \
        --from-beginning --zookeeper localhost:2181

**NOTE** This works with the new Docker for Mac client, which forwards the ports to `localhost`,
this needs testing in a non-dev environment.
If connecting to Kafka from the dev machine (as opposed to another container, eg the API server)
then use `ADVERTISED_HOST=localhost` and the `-h kafka` is unnecessary (and possibly misleading).


## Java Clients

The main application is a REST API server, based on the [Spring Boot](https://spring.io/guides/gs/spring-boot/)
barebone server, built via Gradle.

### Build

To compile and build the packaged JAR application:

    $ ./gradlew assemble

To build the latest image use:

    $ docker build -t massenz/apiserver:${VERSION} -f api/ApiServer.Dockerfile .

To launch use:

    $ docker run --rm --link voldemort --link kafka -h apiserver -P \
        --name apiserver -it massenz/apiserver:${VERSION}

The server will be availabe on `http://docker.local:7779`; however, depending on where you are runing the docker daemon
you may need to forward the port to your local dev machine (see `server.port` in
`application.yaml`).

The `$VERSION` value can be derived from `gradle.properties` via:

    $ export VERSION=$(cat gradle.properties | grep version | cut -f 2 -d '=')

**beware of spaces around the '=' and the version number (there should be none)**


#### Running from IntelliJ

This is based on [this article about Spring Boot and Docker](https://medium.com/@lhartikk/development-environment-in-spring-boot-with-docker-734ad6c50b34) - essentially, just run all containers, then connect to the running API server by running the
`org.springframework.boot.devtools.RemoteSpringApplication` class and using `http://docker.local:7779`
as the *program argument* (adapt host/port to your dev environment).

**Note** I was not able to get the debugging session going following that article.

To build/run all containers from a shell:

    $ ./docker_run.sh

and to stop (and remove them), from another shell:

    $ docker_stop.sh


### Endpoints

**Create a new Issue**

    POST /api/v1/issue

    {
        "reporter": "marco",
        "title": "A bug bit me",
        "comments": [
            {
                "content": "This is a serious problem",
                "commenter": "marco"
            }
        ]
    }

*Response*

    {
      "id": "b8eafacf-3dcf-4807-a5db-9459baee654f",
      "type": "BUG",
      "reporter": "marco",
      "assignee": "",
      "watchers": [],
      "title": "A bug bit me",
      "comments": [
        {
          "content": "This is a serious problem",
          "commenter": "marco",
          "created": 1459890623126
        }
      ],
      "created": 1459890623131,
      "updated": 1459890623131
    }


**Retrieve an Issue**

    GET /api/v1/issue/b8eafacf-3dcf-4807-a5db-9459baee654f

*Response*

    {
      "id": "b8eafacf-3dcf-4807-a5db-9459baee654f",
      "type": "BUG",
      "reporter": "marco",
      "assignee": "",
      "watchers": [],
      "title": "A bug bit me",
      "comments": [
        {
          "content": "This is a serious problem",
          "commenter": "marco",
          "created": 1459890623126
        }
      ],
      "created": 1459890623131,
      "updated": 1459890623131
    }

if the ID refers to a non-existing issue:

    {
      "timestamp": 1459890761257,
      "status": 500,
      "error": "Internal Server Error",
      "exception": "org.springframework.web.client.HttpClientErrorException",
      "message": "org.springframework.web.util.NestedServletException: Request processing failed;
                  nested exception is org.springframework.web.client.HttpClientErrorException:
                  404 Could not find issue [b8eafacf-3dcf-4807--9459baee654f]",
      "path": "/api/v1/issue/b8eafacf-3dcf-4807--9459baee654f"
    }

**TODO** Replace the 500 error code with a proper 404 (although, this is friendlier to an AJAX client).

**Post a new Comment**

    POST /api/v1/issue/[ID]/comment?track=true

    {
        "content": "This is working as intended",
        "commenter": "luisa"
    }

The `track=true` will add the `commenter` to the `watchers` field:

    {
      "id": "b8eafacf-3dcf-4807-a5db-9459baee654f",
      "type": "BUG",
      "reporter": "marco",
      "assignee": "",
      "watchers": [
        "luisa"
      ],
      "title": "A bug bit me",
      "comments": [
        {
          "content": "This is a serious problem",
          "commenter": "marco",
          "created": 1459890623126
        },
        {
          "content": "This is working as intended",
          "commenter": "luisa",
          "created": 1459890905483
        }
      ],
      "created": 1459890623131,
      "updated": 1459890623131
    }



## Pluggable Policies

    TODO
