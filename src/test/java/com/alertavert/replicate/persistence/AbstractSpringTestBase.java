package com.alertavert.replicate.persistence;

import com.alertavert.replicate.AppConfiguration;
import com.alertavert.replicate.RestConfiguration;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * <h1>KafkaReplicatorTest</h1>
 *
 * <p>Insert description here.
 *
 * @author mmassenzio@apple.com, 4/7/17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {
        AppConfiguration.class,
        RestConfiguration.class
})
@ActiveProfiles("test")
public abstract class AbstractSpringTestBase {


    @ClassRule
    public static KafkaEmbedded kafka = new KafkaEmbedded(1, true, 1,
            "test-debug");


    @BeforeClass
    public static void init() {
        System.setProperty("kafka.servers", kafka.getBrokersAsString());

    }
}
