package com.alertavert.replicate.persistence;

import com.alertavert.replicate.model.Issue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * <h1>KafkaReplicatorTest</h1>
 *
 * <p>Insert description here.
 *
 * @author mmassenzio@apple.com, 4/7/17
 */
public class KafkaReplicatorTest extends AbstractSpringTestBase {

    @Autowired
    ReplicatorService<Issue> replicatorService;

    @Test
    public void testCreate() {
        assertNotNull(replicatorService);
    }
}
