// Copyright Marco Massenzio (c) 2014.
// This code is licensed according to the terms of the Apache 2 License.
// See http://www.apache.org/licenses/LICENSE-2.0

package com.alertavert.replicate.resources;

import com.alertavert.replicate.errors.NotFoundError;
import com.alertavert.replicate.model.Comment;
import com.alertavert.replicate.model.Issue;
import com.alertavert.replicate.persistence.ReplicatingStoreOperations;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Layer - sits between the REST API and the Data layer.
 *
 * Created by marco on 12/23/14.
 */
@Service
public class IssuesService {

    private final ReplicatingStoreOperations<Issue> issueRepository;

    @Autowired
    public IssuesService(ReplicatingStoreOperations<Issue> issueRepository) {
        this.issueRepository = issueRepository;
    }

    public Optional<Issue> getIssue(String issueId) {
        return issueRepository.retrieve(issueId);
    }

    public Optional<Issue> addComment(String issueId, Comment comment, boolean shouldWatch) {
        return issueRepository.retrieve(issueId)
                .map(found -> {
                    found.addComment(comment, shouldWatch);
                    issueRepository.store(found.getId(), found);
                    return found;
                });
    }

    public List<Issue> getAllIssues() {
        return new ArrayList<>(issueRepository.retrieveAll(""));
    }

    public void save(Issue issue) {
        issue.setUpdated(new Date());
        issueRepository.store(issue.getId(), issue);
    }
}
