package com.alertavert.replicate.errors;

import com.alertavert.replicate.persistence.ReplicatingStoreOperations;

/**
 * <h1>NotFoundError</h1>
 *
 * <p>Used to signal the looked up key is missing.
 *
 * <p>This should only be used in those situations when an value is expected: when the missing
 * value is a legitimate eventuality, prefer the used of an {@link java.util.Optional} instead.
 *
 * @see ReplicatingStoreOperations
 */
public class NotFoundError extends RuntimeException {
}
