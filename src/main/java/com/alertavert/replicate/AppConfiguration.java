// Copyright Marco Massenzio (c) 2014.
// This code is licensed according to the terms of the Apache 2 License.
// See http://www.apache.org/licenses/LICENSE-2.0

package com.alertavert.replicate;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * Main Configuration class.
 * <p>
 * <p>Use this class to store all application configuration values, bean and other configuration
 * utilities
 * <p>
 * <p>Created by mmassenzio on 12/2/14.
 */
@Configuration
@ComponentScan("com.alertavert.replicate")
public class AppConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(AppConfiguration.class);

    private static final String KAFKA_SERVERS = "bootstrap.servers";
    private static final String KAFKA_STRING_SERIALIZER = "org.apache.kafka.common.serialization.StringSerializer";
    private static final String KAFKA_STRING_DESERIALIZER = "org.apache.kafka.common.serialization.StringDeserializer";

    // TODO: extract out to a StorageConfiguration class
    @Value("@{db.hosts:localhost}")
    String hosts;

    @Value("${db.name:}")
    String databaseName;

    @Value("${db.username:}")
    String username;

    @Value(value = "${db.password:}")
    String password;

    // end StorageConfiguration

    // TODO: extract to a KafkaConfiguration class
    @Value("${kafka.servers}")
    String kafkaServers;

    @Value("${kafka.topic:issues_replicate}")
    String topic;

    @Value("${kafka.should_wait:true}")
    Boolean shouldBlock;

    @Value("${kafka.origin_location}")
    String originLocation;

    @Value("${kafka.poll_timeout}")
    long pollTimeout;

    /**
     * Must be a unique number for each listener, so we replicate the pub-sub semantics.
     *
     * @see KafkaConsumer
     */
    @Value("${random.value}")
    String kafkaGroupId;

    // end KafkaConfiguration


    public Properties producerProps() {
        Properties props = new Properties();

        // TODO(marco): get all this stuff from the application.yaml.
        LOG.info("Connectiong to Kafka server: {}", kafkaServers);
        props.put(KAFKA_SERVERS, kafkaServers);
        props.put("key.serializer", KAFKA_STRING_SERIALIZER);
        props.put("value.serializer", KAFKA_STRING_SERIALIZER);


        // TODO: refactor into configuration properties
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);

        return props;
    }

    public Properties consumerProps() {
        Properties props = new Properties();
        props.put(KAFKA_SERVERS, kafkaServers);
        props.put("key.deserializer", KAFKA_STRING_DESERIALIZER);
        props.put("value.deserializer", KAFKA_STRING_DESERIALIZER);

        props.put("group.id", kafkaGroupId);

        // TODO: refactor into configuration properties
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");

        return props;
    }

    // TODO: create a Cassandra Cluster object and connect to it.


    public String getHosts() {
        return hosts;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getKafkaServers() {
        return kafkaServers;
    }

    public String getTopic() {
        return topic;
    }

    public Boolean getShouldBlock() {
        return shouldBlock;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public long getPollTimeout() {
        return pollTimeout;
    }

    public String getKafkaGroupId() {
        return kafkaGroupId;
    }
}
