package com.alertavert.replicate.persistence;

import java.util.function.BiFunction;

/**
 * <h1>ReplicatorService</h1>
 *
 * <p>A service that replicates values across clusters, using some transport layer.
 */
public interface ReplicatorService<T> {

    /**
     * Replicates the value over a managed Message Bus.
     *
     * @param value the value to replicate.
     * @param key the key for the value.
     */
    public abstract void replicate(String key, T value);

    /**
     * Registers the method to use to store a value that is being replicated across the Message Bus.
     *
     * @param storeMethod the method to invoke to persist the values sent across the Bus: it
     * takes two arguments: the `key` (an arbitrary string that was passed as
     * the Key when replicating) and the `value` to persist.
     */
    public abstract void registerPersistentStoreMethod(BiFunction<String, T, Void> storeMethod);

    /**
     * Optional method, if the replicator starts in a non-replicating mode and should be started
     * to enable replication across the bus.
     *
     * Implementations can just ignore this, if they are "always-on".
     */
    public abstract void starListening();

    /**
     * Mandatory method that should signal the Message Bus that the client no longer wishes to
     * replicate values, even as it <strong>may</strong> continue to invoke the
     * {@link #replicate(String, Object)} method.
     *
     * "Always-on" replicator implementations can "drop" values after this method is called, but
     * should <strong>not</strong> replicate them across the Bus.
     */
    public abstract void stopListening();

    public abstract boolean isListening();
}
