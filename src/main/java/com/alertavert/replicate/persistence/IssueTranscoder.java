package com.alertavert.replicate.persistence;

import com.alertavert.replicate.model.Issue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.spy.memcached.CachedData;
import net.spy.memcached.transcoders.Transcoder;

import java.io.IOException;

import static net.spy.memcached.CachedData.MAX_SIZE;

/**
 * <h1>IssueTranscoder</h1>
 *
 * <p>Insert description here.
 *
 * @author Marco Massenzio (mmassenzio@apple.com)
 */
public class IssueTranscoder implements Transcoder<Issue> {

    static ObjectMapper mapper = new ObjectMapper();

    @Override
    public boolean asyncDecode(CachedData d) {
        return false;
    }

    @Override
    public CachedData encode(Issue issue) {

        try {
            return new CachedData(0, mapper.writeValueAsBytes(issue), MAX_SIZE);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Issue decode(CachedData d) {
        try {
            return mapper.readValue(d.getData(), Issue.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getMaxSize() {
        return MAX_SIZE;
    }
}
