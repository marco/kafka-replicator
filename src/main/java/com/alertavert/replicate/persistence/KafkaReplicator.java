package com.alertavert.replicate.persistence;

import com.alertavert.replicate.AppConfiguration;
import com.alertavert.replicate.model.Issue;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiFunction;

/**
 * <h1>KafkaReplicator</h1>
 *
 * <p>Implements a data replication service using Kafka as the transport layer.
 */
@Service
public class KafkaReplicator implements ReplicatorService<Issue> {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaReplicator.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private final Producer<String, String> producer;

    private final Consumer<String, String> consumer;

    private String topic;

    private Boolean shouldBlock;

    private String originLocation;

    private long pollTimeout;

    private boolean stopped = true;

    private BiFunction<String, Issue, Void> storeMethod;

    @Autowired
    public KafkaReplicator(AppConfiguration configuration) {
        this.producer = new KafkaProducer<String, String>(configuration.producerProps());
        this.consumer = new KafkaConsumer<String, String>(configuration.consumerProps());

        this.topic = configuration.getTopic();
        this.shouldBlock = configuration.getShouldBlock();
        this.originLocation = configuration.getOriginLocation();
        this.pollTimeout = configuration.getPollTimeout();
    }

    @Override
    public void replicate(String key, Issue value) {
        ProducerRecord<String, String> record = new ProducerRecord<String, String>(
                topic,
                String.format("%s:%s", key, originLocation),
                // TODO: serialize the object as a Protobuf instead
                value.toString()
        );
        LOG.info("Posting to Kafka queue [{}; key: {}]", topic, originLocation);
        Future<RecordMetadata> future = producer.send(record);
        try {
            if (shouldBlock) {
                // TODO(marco): use a configured timeout
                long offset = future.get().offset();
                LOG.info("Data posted for replication to offset: {}", offset);
                // TODO(marco): commit the latest successful offset to a log journal.
            } else {
                LOG.debug("Not blocking, data posted to Kafka, failures will not be detected.");
            }
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Could not post to Kafka queue: {}", e);
        }
    }

    @Override
    public void registerPersistentStoreMethod(BiFunction<String, Issue, Void> storeMethod) {
        this.storeMethod = storeMethod;
    }

    @Override
    public void starListening() {
        Objects.requireNonNull(storeMethod);
        LOG.info("Replicator listener started for {}", topic);
        consumer.subscribe(Collections.singleton(topic));
        stopped = false;
        Thread t = new Thread(() -> {
            while (!stopped) {
                ConsumerRecords<String, String> records = consumer.poll(pollTimeout);
                for (ConsumerRecord<String, String> record : records) {
                    String[] split = record.key().split(":");
                    if (!split[1].equals(originLocation)) {
                        LOG.info("Replicating record at offset {}, sent from: {}",
                                record.offset(), record.key());
                        // TODO: implement functionality
                        Issue issue = convertFromString(record.value());
                        storeMethod.apply(split[0], issue);
                    } else {
                        LOG.debug("Skipping same location");
                    }
                }
            }
            LOG.warn("Consumer stopped, replication disabled.");
        });
        t.start();
    }

    // TODO: implement deserialization
    private Issue convertFromString(String value) {
        return Issue.builder("...", "???").newBug();
    }

    @Override
    public void stopListening() {
        stopped = true;
    }

    @Override
    public boolean isListening() {
        return !stopped;
    }
}
