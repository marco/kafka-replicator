package com.alertavert.replicate.persistence;

import java.util.List;
import java.util.Optional;

/**
 * <h1>ReplicatingStoreOperations</h1>
 *
 * <p>The basic interface for a persistence layer that enables replication.
 */
public interface ReplicatingStoreOperations<V> {

    void registerReplicatingService(ReplicatorService<V> replicatorService);

    void store(String key, V value);

    Optional<V> retrieve(String key);

    void delete(String key);

    List<V> retrieveAll(String key);
}
