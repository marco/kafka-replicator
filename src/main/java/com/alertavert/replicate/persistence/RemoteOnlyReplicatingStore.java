package com.alertavert.replicate.persistence;

import com.alertavert.replicate.model.Issue;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * <h1>RemoteOnlyReplicatingStore</h1>
 *
 * <p>Insert description here.
 *
 * @author mmassenzio@apple.com, 4/7/17
 */
@Component
public class RemoteOnlyReplicatingStore implements ReplicatingStoreOperations<Issue> {

    private ReplicatorService<Issue> replicatorService;


    @Override
    public void registerReplicatingService(ReplicatorService<Issue> replicatorService) {
        this.replicatorService = replicatorService;
    }

    @Override
    public void store(String key, Issue value) {
        replicatorService.replicate(key, value);
    }

    @Override
    public Optional<Issue> retrieve(String key) {
        return null;
    }

    @Override
    public void delete(String key) {

    }

    @Override
    public List<Issue> retrieveAll(String key) {
        return null;
    }
}
