// Copyright Marco Massenzio (c) 2014.
// This code is licensed according to the terms of the Apache 2 License.
// See http://www.apache.org/licenses/LICENSE-2.0

package com.alertavert.replicate.api;

import com.alertavert.replicate.RestConfiguration;
import com.alertavert.replicate.errors.NotFoundError;
import com.alertavert.replicate.model.Comment;
import com.alertavert.replicate.model.Issue;
import com.alertavert.replicate.persistence.ReplicatorService;
import com.alertavert.replicate.resources.IssuesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

/**
 * The main API controller
 *
 * Created by marco on 12/23/14.
 */
@RestController
@RequestMapping(RestConfiguration.BASE_URI)
public class ApiController {

    private static final Logger LOG = LoggerFactory.getLogger(ApiController.class);

    public enum ReplicatorAction {
        START, STOP;
    }

    private final IssuesService service;

    @Autowired
    public ApiController(IssuesService service) {
        this.service = service;
    }

    /**
     * Add a new comment to an existing issue.
     *
     * @param id the issue ID
     * @param comment the comment
     * @param track whether the user wants to be notified of updates to the issue
     *
     * @return the newly modified issue
     */
    @RequestMapping(value = "/issue/{id}/comment", method = RequestMethod.POST)
    public Issue commentIssue(@NotNull @PathVariable String id,
                              @RequestBody Comment comment,
                              @RequestParam(required = false, defaultValue = "true") boolean track) {
        LOG.debug("Adding comment to issue {}", id);
        return service.addComment(id, comment, track)
                .orElseThrow(() ->
                        new HttpClientErrorException(
                                HttpStatus.NOT_FOUND,
                                String.format("Could not add comment to issue [%s]", id)));
    }

    /**
     * Creates a new issue.
     *
     * @param newIssue a JSON-formatted new issue.
     *
     * @return the newly created issue.
     */
    @RequestMapping(value = "/issue", method = RequestMethod.POST)
    public Issue createIssue(@NotNull @RequestBody Issue newIssue) {
        UUID newId = UUID.randomUUID();
        LOG.debug("Creating new issue [{}]: {}", newId, newIssue.getTitle());

        newIssue.setId(newId.toString());
        newIssue.setCreated(new Date());
        service.save(newIssue);

        return newIssue;
    }

    /**
     * Retrieves an existing issue, if it exists.
     *
     * @param id the issue's unique identifier
     *
     * @return the issue which as `id` as the {@link Issue#id}
     */
    @RequestMapping(value = "/issue/{id}", method = RequestMethod.GET)
    public Issue getIssue(@PathVariable String id) {
        return service.getIssue(id)
                .orElseThrow(() ->
                        new HttpClientErrorException(
                                HttpStatus.NOT_FOUND,
                                String.format("Could not find issue [%s]", id)));
    }
}
