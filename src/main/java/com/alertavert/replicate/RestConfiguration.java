// Copyright Marco Massenzio (c) 2015.
// This code is licensed according to the terms of the Apache 2 License.
// See http://www.apache.org/licenses/LICENSE-2.0

package com.alertavert.replicate;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

/**
 * General REST configuration goes here.
 *
 * @author marco
 */
@Configuration
public class RestConfiguration extends RepositoryRestConfigurerAdapter {

    public static final String BASE_URI = "/api/v1";

}
