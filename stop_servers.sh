#!/bin/bash

echo "Stopping and removing all containers..."
docker rm -f apiserver apiserver.remote kafka voldemort voldemort.remote voldemort.applicator >/dev/null 2>&1

echo "All containers should be gone now:"
docker ps -a
